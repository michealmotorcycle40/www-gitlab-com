---
layout: handbook-page-toc
title: "Root Cause Analysis for Critical Vulnerabilities"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Root Cause Analysis for Critical Vulnerabilities

## What is an RCA?

Root Cause Analysis (RCA) is a process to ultimately identify the root problem of an issue so that we may prevent it from occurring again. You can learn more about [RCAs here](/handbook/engineering/root-cause-analysis/).

## Why do a RCA for S1 vulnerabilities?

It's important to learn from our past mistakes in order to prevent the same or similar `~"severity::1"` issues from repeating in the future. The expectation is that we can both identify and address the root problem as well as discover other similar attack vectors related to the root cause.

## What are we trying to achieve with RCAs?

The main results we are looking to achieve are the following:

* What was the root cause and why?
  * Was the root cause already identified elsewhere (threat model, previous similar vulnerability, security tools, etc.) and if so how could we get that information to the appropriate people next time?

* What other similar attack vectors does appsec need to review based on this finding?
  * Followup actions:
    1. Brainstorm/threat model on other related attack vectors to review
    1. Do the followup reviews
    1. Identify and create issues on any new findings

* Besides the immediate patch for the `~"severity::1"`, what other changes should be made to address the root problem? 
  * Possible followup actions (not an exhaustive list):
    1. Create issues to address the deeper problem
    1. Create issues for additional defense-in-depth measures
    1. Create a [custom SAST rule](https://docs.gitlab.com/ee/user/application_security/sast/#customize-rulesets)
    1. Create a new detection rule in the [GitLab Inventory Builder](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/gib/)
    1. Add to the secure coding guidelines in the [GitLab Development Documentation](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html)

Once the above questions are answered and followup actions taken, we can consider the RCA a success and can be closed.

## When to start a RCA?

As soon as an `~"severity::1"` security issue is identified.

## Who starts the a RCA?

The appsec team is responsible for initiating the RCA process for any discovered `~"severity::1"` security vulnerabilities. Specifically, the appsec engineer who is assigned to the issue.
 
## Where can I find past RCAs?

In the [appsec-rcas](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-rcas) project.

## How is a RCA initiated?

An RCA starts after a new ~"severity::1" issue has been mitigated. It is initiated by opening an issue in the [appsec-rcas](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-rcas) project using the RCA issue template.

## When is an RCA considered complete?

The RCA is considered complete when the tasks in the RCA issue are marked as completed and the issue is closed. This means that the root cause of the vulnerability is well understood and we have a path forward to reduce the likelihood of a similar vulnerability happening again. For example this can be a [custom SAST rule](https://docs.gitlab.com/ee/user/application_security/sast/#customize-rulesets), new security enhancement addressing the vulnerability class holistically, secure coding training, threat model, more secure application settings, etc.).

