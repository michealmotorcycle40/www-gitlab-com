---
layout: handbook-page-toc
title: "Mutual Close Plan"
description: "A mutual close plan is jointly developed with and agreed upon by the customer and outlines a timeline of key milestones and activities that need to be completed by both parties to get the deal through closure and into deployment"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
A mutual close plan is jointly developed with and agreed upon by the customer (with champions, advocates, and economic buyers) and outlines a timeline of key milestones and activities that need to be completed by both parties (including when and by whom) to get the deal through closure and into deployment. The creation of an explicit mutual close plan will help identify gaps in understanding and/or potential obstacles in the procurement process so you may proactively formulate plans with the customer to mitigate unexpected delays, improve your forecast accuracy, and accelerate the customer's time to value. Access the [sample mutual close plan](https://docs.google.com/presentation/d/1noScuDwweIX_rBSqvPqaTR5ag4GbD9e8WqE5x7_OqJ4/edit?usp=sharing).

<!-- blank line -->
<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR1m7yf5Db9zE-dt9papH28yHBM3XH-Tm3iCHYgySiNlK97RSZJ0RUXah29zZpnuhYf4wxq8B9iK6gl/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>
<!-- blank line -->
